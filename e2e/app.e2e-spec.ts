import { WebDroneRemotePage } from './app.po';

describe('web-drone-remote App', () => {
  let page: WebDroneRemotePage;

  beforeEach(() => {
    page = new WebDroneRemotePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
