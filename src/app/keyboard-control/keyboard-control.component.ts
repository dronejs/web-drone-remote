import { Component, OnInit, Input } from '@angular/core';
import { SocketServiceService } from '../socket-service.service';
import { HostListener } from '@angular/core';


@Component({
  selector: 'app-keyboard-control',
  templateUrl: './keyboard-control.component.html',
  styleUrls: ['./keyboard-control.component.scss'],
  providers: [SocketServiceService]
})
export class KeyboardControlComponent implements OnInit {


  @Input() socket;
  handle;

  allowedKeys: object = {
    'a':{
      name: 'roll left',
      command: {
        x: -1,
        y: 0,
        z: 0
      }
    },
    'w': {
      name: 'pitch forward',
      command: {
        x: 0,
        y: 0,
        z: 1
      }
    },
    'd': {
      name: 'roll right',
      command: {
        x: 1,
        y: 0,
        z: 0
      }
    },
    's': {
      name: 'pitch backward',
      command: {
        x: 0,
        y: 0,
        z: -1
      }
    },
    'u': {
      name: 'increase throttle',
      command: {
        x: 0,
        y: 1,
        z: 0
      }
    },
    'j': {
      name: 'decrease throttle',
      command: {
        x: 0,
        y: -1,
        z: 0
      }
    },
    'p': {
      name: 'Kill engines',
      command: {
        x: 0,
        y: 0,
        z: 0
      }
    }
  };
  constructor() {}

  @HostListener('window:keydown', ['$event'])
  keyboardInput(event: KeyboardEvent) {
    if (Object.keys(this.allowedKeys).indexOf(event.key) >= 0) {
        this.sendMessage(event.key);
    }
  }

  sendMessage(key: string) {
    this.socket.send({
      command: 'joystick',
      payload: this.allowedKeys[key]['command']
    });
  }



  ngOnInit() {


    this.socket.socket.on('handle', handle => {
      this.handle = handle;
    })

  }

}
