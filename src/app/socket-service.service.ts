import { Injectable } from '@angular/core';
import * as socketIo from 'socket.io-client';

const SERVER_URL = 'http://192.168.0.104:8080';

@Injectable()
export class SocketServiceService {

  public socket = socketIo(SERVER_URL);

  constructor() {
  }

  public send(payload: any): void {
    this.socket.emit('command', payload);
  }



}
