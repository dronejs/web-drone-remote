import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html',
  styleUrls: ['./engine.component.css']
})
export class EngineComponent implements OnInit {

  @Input() engine: object;

  constructor() { }

  ngOnInit() {
  }

}
