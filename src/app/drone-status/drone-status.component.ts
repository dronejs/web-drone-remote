import { Component, OnInit, Input } from '@angular/core';
import { SocketServiceService } from '../socket-service.service';

@Component({
  selector: 'app-drone-status',
  templateUrl: './drone-status.component.html',
  styleUrls: ['./drone-status.component.scss'],
  providers: [SocketServiceService]
})
export class DroneStatusComponent implements OnInit {

  @Input() socket;

  engines: object = {};

  constructor() { }


  ngOnInit() {

    this.socket.socket.on('engineStatus', data => {


      this.engines[data.GPIO] = data;
    })

  }

}
