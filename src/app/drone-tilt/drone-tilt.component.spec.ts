import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DroneTiltComponent } from './drone-tilt.component';

describe('DroneTiltComponent', () => {
  let component: DroneTiltComponent;
  let fixture: ComponentFixture<DroneTiltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DroneTiltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DroneTiltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
