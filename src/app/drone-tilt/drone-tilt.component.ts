import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-drone-tilt',
  templateUrl: './drone-tilt.component.html',
  styleUrls: ['./drone-tilt.component.css']
})
export class DroneTiltComponent implements OnInit {

  @Input() socket;
  tilt;


  constructor() { }

  ngOnInit() {


    this.socket.socket.on('tilt', tilt => {
      this.tilt = tilt;
    })

  }

}
