import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SocketServiceService } from './socket-service.service';
import { AppComponent } from './app.component';
import { KeyboardControlComponent } from './keyboard-control/keyboard-control.component';
import { DroneStatusComponent } from './drone-status/drone-status.component';
import { EngineComponent } from './drone-status/engine/engine.component';
import { DroneTiltComponent } from './drone-tilt/drone-tilt.component';

@NgModule({
  declarations: [
    AppComponent,
    KeyboardControlComponent,
    DroneStatusComponent,
    EngineComponent,
    DroneTiltComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [SocketServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
