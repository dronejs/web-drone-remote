import { Component } from '@angular/core';
import { SocketServiceService } from './socket-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';

  socket;

  ngOnInit() {

    this.socket = new SocketServiceService();


  }

}
